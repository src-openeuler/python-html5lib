Name:           python-html5lib
Version:        1.1
Release:        1
Epoch:          1
Summary:	HTML parser based on the WHAT-WG Web Applications 1
License:        MIT
URL:            https://github.com/html5lib/html5lib-python
Source0:        https://github.com/html5lib/html5lib-python/archive/%{version}.tar.gz


BuildArch:      noarch

%?python_enable_dependency_generator

%description
Html5lib is a pure-python library for parsing HTML. It is designed to conform 
to the WHATWG HTML specification, as is implemented by all major web browsers.


%package     -n python3-html5lib
Summary:        python3 package for html5lib
BuildRequires:  python3-devel python3-setuptools python3-mock python3-pytest python3-pytest-expect
BuildRequires:  python3-webencodings python3-chardet python3-genshi python3-lxml python3-six
%{?python_provide:%python_provide python3-html5lib}

%description -n python3-html5lib
python3 package for html5lib.

%package_help

%prep
%autosetup -n html5lib-python-%{version} -p1

%build
%py3_build

%install
%py3_install

%files       -n python3-html5lib
%defattr(-,root,root)
%license LICENSE 
%{python3_sitelib}/*

%files          help
%defattr(-,root,root)
%doc CHANGES.rst README.rst

%changelog
* Wed Jan 27 2021 zhanzhimin <zhanzhimin@huawei.com> - 1.1-1
- update to 1.1

* Thu Sep 10 2020 chengguipeng<chengguipeng1@huawei.com> - 1.0.1-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source0 url

* Wed Jul 15 2020 zhouhaibo <zhouhaibo@huawei.com> - 1.0.1-6
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:delete python2 dependencies

* Sat Mar 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0.1-5
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.1-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.1-3
- Package init
